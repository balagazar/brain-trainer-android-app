package com.example.alexanderlogvinenko.braintrainer;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Random;


/**
 * This app was developed by Alexander Logvinenko on February 8 2018.
 */

public class MainActivity extends AppCompatActivity {


    ArrayList<Integer> answers =new ArrayList<Integer>();
    TextView timeleft;
    CountDownTimer timer;
    int firstNum;
    int secondNum;
    int CurrentScore;
    int Count;
    View GameLayout;
    GridLayout buttons;
    final Random rand = new Random();

    Button button0;
    Button button1;
    Button button2;
    Button button3;

    TextView reaction;



    public void Answer(View view){


        Button btn = (Button)view;
        int answer =  Integer.valueOf(btn.getText().toString());
        if(answer==firstNum+secondNum)
        {
            CurrentScore++;
            reaction.setText("Correct");
        }else{
            reaction.setText("Wrong!");
        }
        Count++;
        UpdateScore();
        GenerateButtons();
    }

    public void GenerateButtons(){
        answers.clear();

        firstNum = rand.nextInt(21);
        secondNum = rand.nextInt(21);



        int answerSpot = rand.nextInt(4);

        for(int i =0; i<4;i++){
            if(i==answerSpot){
                answers.add(firstNum+secondNum);
            }else{
                int falseAnswer = rand.nextInt(41);

                while(falseAnswer==firstNum+secondNum){
                    falseAnswer = rand.nextInt(41);
                }
                answers.add(falseAnswer);
            }

        }

        TextView equation = (TextView) findViewById(R.id.eq);
        equation.setText(firstNum+"+"+secondNum);
        show(equation);

        button0.setText(answers.get(0).toString());
        button1.setText(answers.get(1).toString());
        button2.setText(answers.get(2).toString());
        button3.setText(answers.get(3).toString());


    }


    public void UpdateScore(){
        TextView score = (TextView) findViewById(R.id.scoreView);
        score.setText(CurrentScore+"/"+Count);
    }

    public void stratapp(View view) {


        Button GoBtn = (Button) findViewById(R.id.GoBtn);
        hide(GoBtn);
        GenerateButtons();
        setTimer();
        show(GameLayout);


    }

    public void setTimer(){
        timer = new CountDownTimer(30100, 1000) {
            @Override
            public void onTick(long millisecondsUntilDone) {
                UpdateTimer((int) millisecondsUntilDone / 1000);
            }

            @Override
            public void onFinish() {
                Button playagain = (Button) findViewById(R.id.playAgainBtn);
                show((View) playagain);
                reaction.setText("Your score is: "+CurrentScore+"/"+Count);
                hide(buttons);
            }
        }.start();
    }

    public void UpdateTimer(int second){
        timeleft.setText(second+"s");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GameLayout = (View) findViewById(R.id.GameLayout);
        timeleft = (TextView) findViewById(R.id.secondsLeftView);
        button0 = (Button) findViewById(R.id.btn1);
        button1 = (Button) findViewById(R.id.btn2);
        button2 = (Button) findViewById(R.id.btn3);
        button3 = (Button) findViewById(R.id.btn4);
        reaction = (TextView) findViewById(R.id.ReactionView);
        buttons = (GridLayout) findViewById(R.id.GridLayout);

        CurrentScore=0;
        Count=0;


    }
    public void playagain(View view){
        GenerateButtons();
        CurrentScore=0;
        Count=0;
        UpdateScore();
        timer.start();
        hide((View) view);
        show(buttons);
        reaction.setText("");
    }


    public void show(View view){
        view.setVisibility(View.VISIBLE);
    }
    public void hide(View view){
        view.setVisibility(View.INVISIBLE);
    }
}
